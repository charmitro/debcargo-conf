From 7f933e81d8046f8c22fe9d106dabb0b0933c5a3f Mon Sep 17 00:00:00 2001
From: Sergio Lopez <slp@redhat.com>
Date: Fri, 28 Apr 2023 15:50:01 +0200
Subject: [PATCH] rand: fix tests on big-endian machines

Some methods in rand.rs use "to_ne_bytes()", which yields different
results between little endian and big endian machines. We need to modify
some tests to be aware of this and avoid failure on big endian machines.

Signed-off-by: Sergio Lopez <slp@redhat.com>
---
 src/rand.rs | 24 ++++++++++++++++++++----
 1 file changed, 20 insertions(+), 4 deletions(-)

diff --git a/src/rand.rs b/src/rand.rs
index 98047a8..8a88dd3 100644
--- a/src/rand.rs
+++ b/src/rand.rs
@@ -124,13 +124,21 @@ mod tests {
                          // The 33 will be discarded as it is not a valid letter
                          // (upper or lower) or number.
         let s = xor_pseudo_rng_u8_alphanumerics(&|| i);
-        assert_eq!(vec![54, 55], s);
+        if cfg!(target_endian = "big") {
+            assert_eq!(vec![55, 54], s);
+        } else {
+            assert_eq!(vec![54, 55], s);
+        }
     }
 
     #[test]
     fn test_rand_alphanumerics_impl() {
         let s = rand_alphanumerics_impl(&|| 14134, 5);
-        assert_eq!("67676", s);
+        if cfg!(target_endian = "big") {
+            assert_eq!("76767", s);
+        } else {
+            assert_eq!("67676", s);
+        }
     }
 
     #[test]
@@ -145,13 +153,21 @@ mod tests {
                          // The 33 will be discarded as it is not a valid letter
                          // (upper or lower) or number.
         let s = xor_pseudo_rng_u8_bytes(&|| i);
-        assert_eq!(vec![54, 33, 55, 0], s);
+        if cfg!(target_endian = "big") {
+            assert_eq!(vec![0, 55, 33, 54], s);
+        } else {
+            assert_eq!(vec![54, 33, 55, 0], s);
+        }
     }
 
     #[test]
     fn test_rand_bytes_impl() {
         let s = rand_bytes_impl(&|| 1234567, 4);
-        assert_eq!(vec![135, 214, 18, 0], s);
+        if cfg!(target_endian = "big") {
+            assert_eq!(vec![0, 18, 214, 135], s);
+        } else {
+            assert_eq!(vec![135, 214, 18, 0], s);
+        }
     }
 
     #[test]
