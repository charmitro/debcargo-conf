rust-terminal-size (0.2.6-2) experimental; urgency=medium

  * Package terminal_size 0.2.6 from crates.io using debcargo 2.6.0

  [ Fabian Grünbichler ]
  * Team upload.
  * bump rustix to 0.38

 -- Peter Michael Green <plugwash@debian.org>  Sun, 27 Aug 2023 19:28:33 +0000

rust-terminal-size (0.2.6-1) unstable; urgency=medium

  * Team upload.
  * Package terminal_size 0.2.6 from crates.io using debcargo 2.6.0
  * Fix some undefined behaviour in create-terminal-for-testing.patch
  * Update patches for new upstream.

 -- Peter Michael Green <plugwash@debian.org>  Tue, 13 Jun 2023 16:25:40 +0000

rust-terminal-size (0.2.1-3) unstable; urgency=medium

  * Team upload.
  * Package terminal_size 0.2.1 from crates.io using debcargo 2.5.0
  * Add breaks on old version of librust-textwrap-dev to help Britney's
    autopkgtest scheduler.

 -- Peter Michael Green <plugwash@debian.org>  Tue, 01 Nov 2022 05:47:11 +0000

rust-terminal-size (0.2.1-2) unstable; urgency=medium

  * Team upload.
  * Package terminal_size 0.2.1 from crates.io using debcargo 2.5.0
  * Add a dev-dependency on libc, terminal-size has switched from libc
    to rustix, but my code to create a terminal for testing still needs
    libc (and rustix does not seem to offer equivilent functionality).
  * Stop overriding debian/rules.

 -- Peter Michael Green <plugwash@debian.org>  Sun, 30 Oct 2022 21:54:57 +0000

rust-terminal-size (0.2.1-1) unstable; urgency=medium

  * Team upload.
  * Package terminal_size 0.2.1 from crates.io using debcargo 2.5.0

 -- Blair Noctis <n@sail.ng>  Sun, 30 Oct 2022 09:24:32 +0100

rust-terminal-size (0.1.17-2) unstable; urgency=medium

  * Team upload.
  * Package terminal_size 0.1.17 from crates.io using debcargo 2.4.4
  * When creating pseudo-terminal for testing use c_char instead of i8
    for the array passed to ptsname_r so the test code will build on
    architectures with unsigned char.

 -- Peter Michael Green <plugwash@debian.org>  Sat, 04 Sep 2021 23:53:42 +0000

rust-terminal-size (0.1.17-1) unstable; urgency=medium

  * Team upload.
  * Package terminal_size 0.1.17 from crates.io using debcargo 2.4.4
  * Create a psuedo-terminal to use for testing rather than relying on the
    environment providing a terminal (which Debian's autopkgtest environment 
    doesn't).

 -- Peter Michael Green <plugwash@debian.org>  Sat, 04 Sep 2021 18:33:24 +0000

rust-terminal-size (0.1.13-2) unstable; urgency=medium

  * Team upload.
  * Source upload
  * Package terminal_size 0.1.13 from crates.io using debcargo 2.4.4-alpha.0

 -- Sylvestre Ledru <sylvestre@debian.org>  Thu, 14 Jan 2021 10:39:59 +0100

rust-terminal-size (0.1.13-1) unstable; urgency=medium

  * Source upload
  * Package terminal_size 0.1.13 from crates.io using debcargo 2.4.2

 -- Alois Micard <alois@micard.lu>  Sun, 18 Oct 2020 00:51:11 +0200
